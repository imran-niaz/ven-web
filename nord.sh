[+] URL: https://nordvpn.com/ [104.17.49.74]
[+] Started: Tue Mar 21 13:56:39 2023

Interesting Finding(s):

[+] Headers
 | Interesting Entries:
 |  - cf-ray: 7abb0af00c224122-ISB
 |  - cf-cache-status: HIT
 |  - x-generator: front-kr-web-1
 |  - server: cloudflare
 |  - alt-svc: h3=":443"; ma=86400, h3-29=":443"; ma=86400
 | Found By: Headers (Passive Detection)
 | Confidence: 100%

[+] robots.txt found: https://nordvpn.com/robots.txt
 | Found By: Robots Txt (Aggressive Detection)
 | Confidence: 100%

Fingerprinting the version - Time: 00:01:58 <======================================================================================> (695 / 695) 100.00% Time: 00:01:58
[i] The WordPress version could not be detected.

[i] The main theme could not be detected.

[+] Enumerating Vulnerable Plugins (via Passive and Aggressive Methods)
 Checking Known Locations - Time: 00:29:18 <=====================================================================================> (5308 / 5308) 100.00% Time: 00:29:18
[+] Checking Plugin Versions (via Passive and Aggressive Methods)

[i] Plugin(s) Identified:

[+] admin-columns-pro
 | Location: https://nordvpn.com/wp-content/plugins/admin-columns-pro/
 |
 | Found By: Known Locations (Aggressive Detection)
 |  - https://nordvpn.com/wp-content/plugins/admin-columns-pro/, status: 403
 |
 | [!] 2 vulnerabilities identified:
 |
 | [!] Title: Admin Columns Free < 4.3 & Pro < 5.5.1 - Authenticated Stored Cross-Site Scripting (XSS) in Label
 |     Fixed in: 5.5.1
 |     References:
 |      - https://wpscan.com/vulnerability/05427156-4d5c-4aeb-add8-1c574fda5c28
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-24366
 |      - https://www.whitesourcesoftware.com/vulnerability-database/CVE-2021-24366
 |
 | [!] Title: Admin Columns Free (< 4.3.2) & Pro (< 5.5.2) - Authenticated Stored Cross-Site Scripting (XSS) in Custom Field
 |     Fixed in: 5.5.2
 |     References:
 |      - https://wpscan.com/vulnerability/fdbeb137-b404-46c7-85fb-394a3bdac388
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-24365
 |      - https://www.syss.de/fileadmin/dokumente/Publikationen/Advisories/SYSS-2021-032.txt
 |
 | The version could not be determined.

[+] akismet
 | Location: https://nordvpn.com/wp-content/plugins/akismet/
 | Latest Version: 5.1
 | Last Updated: 2023-03-20T19:29:00.000Z
 |
 | Found By: Known Locations (Aggressive Detection)
 |  - https://nordvpn.com/wp-content/plugins/akismet/, status: 200
 |
 | [!] 1 vulnerability identified:
 |
 | [!] Title: Akismet 2.5.0-3.1.4 - Unauthenticated Stored Cross-Site Scripting (XSS)
 |     Fixed in: 3.1.5
 |     References:
 |      - https://wpscan.com/vulnerability/1a2f3094-5970-4251-9ed0-ec595a0cd26c
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-9357
 |      - http://blog.akismet.com/2015/10/13/akismet-3-1-5-wordpress/
 |      - https://blog.sucuri.net/2015/10/security-advisory-stored-xss-in-akismet-wordpress-plugin.html
 |
 | The version could not be determined.

[+] all-in-one-seo-pack
 | Location: https://nordvpn.com/wp-content/plugins/all-in-one-seo-pack/
 | Latest Version: 4.3.3
 | Last Updated: 2023-03-16T14:11:00.000Z
 |
 | Found By: Known Locations (Aggressive Detection)
 |  - https://nordvpn.com/wp-content/plugins/all-in-one-seo-pack/, status: 200
 |
 | [!] 16 vulnerabilities identified:
 |
 | [!] Title: All in One SEO Pack <= 2.1.5 - aioseop_functions.php new_meta Parameter XSS
 |     Fixed in: 2.1.6
 |     References:
 |      - https://wpscan.com/vulnerability/845a890f-1c56-4075-a717-c9627586cbbe
 |      - https://blog.sucuri.net/2014/05/vulnerability-found-in-the-all-in-one-seo-pack-wordpress-plugin.html
 |
 | [!] Title: All in One SEO Pack <= 2.1.5 - Unspecified Privilege Escalation
 |     Fixed in: 2.1.6
 |     References:
 |      - https://wpscan.com/vulnerability/3a79a32d-2744-41c5-8752-0540f744e25d
 |      - https://blog.sucuri.net/2014/05/vulnerability-found-in-the-all-in-one-seo-pack-wordpress-plugin.html
 |
 | [!] Title: All in One SEO Pack <= 2.0.3 - XSS 
 |     Fixed in: 2.0.3.1
 |     References:
 |      - https://wpscan.com/vulnerability/0ddc6e3f-8a02-43d9-ba63-e3d94e7b0a1b
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-5988
 |      - https://packetstormsecurity.com/files/123490/
 |      - https://www.securityfocus.com/bid/62784/
 |      - https://seclists.org/bugtraq/2013/Oct/8
 |
 | [!] Title: All in One SEO Pack <= 2.2.5.1 - Information Disclosure
 |     Fixed in: 2.2.6
 |     References:
 |      - https://wpscan.com/vulnerability/de4706de-f388-41b9-af91-bd4677a7166e
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-0902
 |      - https://jvn.jp/en/jp/JVN75615300/index.html
 |      - https://web.archive.org/web/20150809074225/http://semperfiwebdesign.com/blog/all-in-one-seo-pack/all-in-one-seo-pack-release-history/
 |
 | [!] Title: All in One SEO Pack <= 2.2.6.1 - Cross-Site Scripting (XSS)
 |     Fixed in: 2.2.6.2
 |     References:
 |      - https://wpscan.com/vulnerability/9c76c04d-3c4e-4a06-ab7e-054bc21c55fc
 |      - https://blog.sucuri.net/2015/04/security-advisory-xss-vulnerability-affecting-multiple-wordpress-plugins.html
 |
 | [!] Title: All in One SEO Pack <= 2.3.6.1 - Unauthenticated Stored Cross-Site Scripting (XSS)
 |     Fixed in: 2.3.7
 |     References:
 |      - https://wpscan.com/vulnerability/aa790e24-ae9d-48b9-8071-7ca138cdc69d
 |      - https://seclists.org/fulldisclosure/2016/Jul/23
 |      - https://web.archive.org/web/20160508074631/http://semperfiwebdesign.com/blog/all-in-one-seo-pack/all-in-one-seo-pack-release-history/
 |      - https://sumofpwn.nl/advisory/2016/persistent_cross_site_scripting_in_all_in_one_seo_pack_wordpress_plugin.html
 |      - https://wptavern.com/all-in-one-seo-2-3-7-patches-persistent-xss-vulnerability
 |      - https://www.wordfence.com/blog/2016/07/xss-vulnerability-all-in-one-seo-pack-plugin/
 |
 | [!] Title: All in One SEO Pack <= 2.3.7 -  Unauthenticated Stored Cross-Site Scripting (XSS)
 |     Fixed in: 2.3.8
 |     References:
 |      - https://wpscan.com/vulnerability/42a98395-e4e4-47a5-b758-2b76b7c4885a
 |      - https://www.wordfence.com/blog/2016/07/new-xss-vulnerability-all-in-one-seo-pack/
 |      - https://semperfiwebdesign.com/blog/all-in-one-seo-pack/all-in-one-seo-pack-release-history/
 |      - https://web.archive.org/web/20161019215603/https://semperfiwebdesign.com/blog/all-in-one-seo-pack/all-in-one-seo-pack-release-history/
 |
 | [!] Title: All in One SEO Pack <= 2.9.1.1 - Authenticated Stored Cross-Site Scripting (XSS)
 |     Fixed in: 2.10
 |     References:
 |      - https://wpscan.com/vulnerability/16353d45-75d1-4820-b93f-daad90c322a8
 |      - https://www.ripstech.com/php-security-calendar-2018/#day-4
 |      - https://wordpress.org/support/topic/a-critical-vulnerability-has-been-detected-in-this-plugin/
 |      - https://semperfiwebdesign.com/all-in-one-seo-pack-release-history/
 |
 | [!] Title: All In One SEO Pack < 3.2.7 - Stored Cross-Site Scripting (XSS)
 |     Fixed in: 3.2.7
 |     References:
 |      - https://wpscan.com/vulnerability/868dccee-089b-43d2-a80a-6cadba91f770
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-16520
 |      - https://github.com/sbaresearch/advisories/tree/public/2019/SBA-ADV-20190913-04_WordPress_Plugin_All_in_One_SEO_Pack
 |
 | [!] Title: All in One SEO Pack < 3.6.2 - Authenticated Stored Cross-Site Scripting
 |     Fixed in: 3.6.2
 |     References:
 |      - https://wpscan.com/vulnerability/528fff6c-54fe-4812-9b08-8c4e47350c83
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-35946
 |      - https://www.wordfence.com/blog/2020/07/2-million-users-affected-by-vulnerability-in-all-in-one-seo-pack/
 |      - https://www.youtube.com/watch?v=2fqMM6HRV5s
 |
 | [!] Title: All in One SEO Pack <  4.1.0.2 - Admin RCE via unserialize
 |     Fixed in: 4.1.0.2
 |     References:
 |      - https://wpscan.com/vulnerability/ab2c94d2-f6c4-418b-bd14-711ed164bcf1
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-24307
 |      - https://aioseo.com/changelog/
 |
 | [!] Title: All In One SEO < 4.1.5.3 - Authenticated SQL Injection
 |     Fixed in: 4.1.5.3
 |     References:
 |      - https://wpscan.com/vulnerability/4cd2a57b-3e1a-4acf-aecb-201ed9f4ee6d
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-25037
 |      - https://jetpack.com/2021/12/14/severe-vulnerabilities-fixed-in-all-in-one-seo-plugin-version-4-1-5-3/
 |      - https://plugins.trac.wordpress.org/changeset/2640944/all-in-one-seo-pack/trunk/app/Common/Api/PostsTerms.php
 |
 | [!] Title: All In One SEO < 4.1.5.3 - Authenticated Privilege Escalation
 |     Fixed in: 4.1.5.3
 |     References:
 |      - https://wpscan.com/vulnerability/6de4a7de-6b71-4349-8e52-04c89c5e6d6c
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-25036
 |      - https://jetpack.com/2021/12/14/severe-vulnerabilities-fixed-in-all-in-one-seo-plugin-version-4-1-5-3/
 |      - https://plugins.trac.wordpress.org/changeset/2640944/all-in-one-seo-pack/trunk/app/Common/Api/Api.php
 |
 | [!] Title: All in One SEO < 4.2.4 - Multiple CSRF
 |     Fixed in: 4.2.4
 |     References:
 |      - https://wpscan.com/vulnerability/5f31b537-186d-424c-a0c3-56f29146bb6e
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-38093
 |
 | [!] Title: All in One SEO Pack < 4.3.0 - Contributor+ Stored XSS
 |     Fixed in: 4.3.0
 |     References:
 |      - https://wpscan.com/vulnerability/e2e78948-81cc-49e4-9a68-1a989a4a0585
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-0586
 |
 | [!] Title: All in One SEO Pack < 4.3.0 - Admin+ Stored XSS
 |     Fixed in: 4.3.0
 |     References:
 |      - https://wpscan.com/vulnerability/3ace429e-42a8-4b1b-8a39-5262f289cbd9
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-0585
 |
 | The version could not be determined.

[+] contact-form-7
 | Location: https://nordvpn.com/wp-content/plugins/contact-form-7/
 | Latest Version: 5.7.4
 | Last Updated: 2023-02-19T04:38:00.000Z
 |
 | Found By: Known Locations (Aggressive Detection)
 |  - https://nordvpn.com/wp-content/plugins/contact-form-7/, status: 403
 |
 | [!] 4 vulnerabilities identified:
 |
 | [!] Title: Contact Form 7 <= 3.7.1 - CAPTCHA Bypass 
 |     Fixed in: 3.7.2
 |     References:
 |      - https://wpscan.com/vulnerability/3020e3f1-162f-48f9-ace3-7ba1c7c6b164
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-2265
 |      - https://www.securityfocus.com/bid/66381/
 |
 | [!] Title: Contact Form 7 <= 3.5.2 - File Upload Remote Code Execution
 |     Fixed in: 3.5.3
 |     References:
 |      - https://wpscan.com/vulnerability/f307ec17-da98-424f-b784-7cb63351aa8e
 |      - https://packetstormsecurity.com/files/124154/
 |
 | [!] Title: Contact Form 7 <= 5.0.3 - register_post_type() Privilege Escalation
 |     Fixed in: 5.0.4
 |     References:
 |      - https://wpscan.com/vulnerability/af945f64-9ce2-485c-bf36-c2ff59dc10d5
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-20979
 |      - https://contactform7.com/2018/09/04/contact-form-7-504/
 |      - https://plugins.trac.wordpress.org/changeset/1935726/contact-form-7
 |      - https://plugins.trac.wordpress.org/changeset/1934594/contact-form-7
 |      - https://plugins.trac.wordpress.org/changeset/1934343/contact-form-7
 |      - https://plugins.trac.wordpress.org/changeset/1934327/contact-form-7
 |      - https://www.ripstech.com/php-security-calendar-2018/#day-18
 |
 | [!] Title: Contact Form 7 < 5.3.2 - Unrestricted File Upload
 |     Fixed in: 5.3.2
 |     References:
 |      - https://wpscan.com/vulnerability/7391118e-eef5-4ff8-a8ea-f6b65f442c63
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-35489
 |      - https://www.getastra.com/blog/911/plugin-exploit/contact-form-7-unrestricted-file-upload-vulnerability/
 |      - https://www.jinsonvarghese.com/unrestricted-file-upload-in-contact-form-7/
 |      - https://contactform7.com/2020/12/17/contact-form-7-532/#more-38314
 |
 | The version could not be determined.

[+] disqus-comment-system
 | Location: https://nordvpn.com/wp-content/plugins/disqus-comment-system/
 | Latest Version: 3.0.22
 | Last Updated: 2021-05-26T21:50:00.000Z
 |
 | Found By: Known Locations (Aggressive Detection)
 |  - https://nordvpn.com/wp-content/plugins/disqus-comment-system/, status: 200
 |
 | [!] 5 vulnerabilities identified:
 |
 | [!] Title: Disqus <= 2.75 - Remote Code Execution (RCE)
 |     Fixed in: 2.76
 |     References:
 |      - https://wpscan.com/vulnerability/ea12a9dd-4df7-4f16-9fe4-1aacc1e036d7
 |      - https://blog.sucuri.net/2014/06/anatomy-of-a-remote-code-execution-bug-on-disqus.html
 |
 | [!] Title: Disqus Comment System <= 2.68 - Reflected Cross-Site Scripting (XSS)
 |     Fixed in: 2.69
 |     References:
 |      - https://wpscan.com/vulnerability/4146cdd7-b25a-4f1e-ad95-697b790466c5
 |      - https://web.archive.org/web/20120214134353/http://www.ethicalhack3r.co.uk/security/wordpress-plugin-disqus-comment-system-xss/
 |
 | [!] Title: Disqus Blog Comments <= 2.77 - Blind SQL Injection 
 |     Fixed in: 2.7.8
 |     References:
 |      - https://wpscan.com/vulnerability/7abdf114-6bdc-4601-820f-4a6f6869bea0
 |      - https://www.exploit-db.com/exploits/20913/
 |
 | [!] Title: Disqus <= 2.77 - Cross-Site Request Forgery (CSRF)
 |     Fixed in: 2.79
 |     References:
 |      - https://wpscan.com/vulnerability/4b706704-1301-4d29-a5e3-2383425ed9a7
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-5346
 |      - https://vexatioustendencies.com/csrf-in-disqus-wordpress-plugin-v2-77/
 |
 | [!] Title: Disqus <= 2.75 - Cross-Site Scripting (XSS) & CSRF
 |     Fixed in: 2.76
 |     References:
 |      - https://wpscan.com/vulnerability/d0efe57b-ede3-4277-856d-61259785a884
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-5345
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-5347
 |      - https://gist.github.com/nikcub/cb5dc7a5464276c8424a
 |
 | The version could not be determined.

[+] drag-and-drop-multiple-file-upload-contact-form-7
 | Location: https://nordvpn.com/wp-content/plugins/drag-and-drop-multiple-file-upload-contact-form-7/
 | Latest Version: 1.3.6.8
 | Last Updated: 2023-03-21T07:59:00.000Z
 |
 | Found By: Known Locations (Aggressive Detection)
 |  - https://nordvpn.com/wp-content/plugins/drag-and-drop-multiple-file-upload-contact-form-7/, status: 403
 |
 | [!] 4 vulnerabilities identified:
 |
 | [!] Title: Drag and Drop Multiple File Upload for Contact Form 7 < 1.3.3.3 - Unauthenticated File Upload Bypass
 |     Fixed in: 1.3.3.3
 |     References:
 |      - https://wpscan.com/vulnerability/53e47e67-c8c9-45a9-9a9c-da52c37047bf
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-12800
 |      - https://www.exploit-db.com/exploits/48520/
 |      - https://packetstormsecurity.com/files/157837/
 |      - https://packetstormsecurity.com/files/157951/
 |      - https://github.com/amartinsec/CVE-2020-12800
 |      - https://www.rapid7.com/db/modules/exploit/multi/http/wp_dnd_mul_file_rce/
 |
 | [!] Title: Drag and Drop Multiple File Upload – Contact Form 7 < 1.3.5.5 - Unauthenticated Remote Code Execution
 |     Fixed in: 1.3.5.5
 |     References:
 |      - https://wpscan.com/vulnerability/78a99a00-48d8-406b-af42-113eb0d379a4
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-24389
 |      - https://sysdream.com/news/lab/2020-09-21-cve-2020-24389-remote-code-execution-on-drag-and-drop-multiple-file-upload-contact-form-7/
 |      - https://plugins.trac.wordpress.org/changeset/2369106/drag-and-drop-multiple-file-upload-contact-form-7
 |
 | [!] Title: Drag and Drop Multiple File Upload - Contact Form 7 < 1.3.6.3 - Unauthenticated Stored XSS
 |     Fixed in: 1.3.6.3
 |     References:
 |      - https://wpscan.com/vulnerability/1b849957-eaca-47ea-8f84-23a3a98cc8de
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-0595
 |      - https://plugins.trac.wordpress.org/changeset/2686614
 |
 | [!] Title: Drag and Drop Multiple File Upload < 1.3.6.5 - File Upload Size Limit Bypass
 |     Fixed in: 1.3.6.5
 |     References:
 |      - https://wpscan.com/vulnerability/035dffef-4b4b-4afb-9776-7f6c5e56452c
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-3282
 |
 | The version could not be determined.

[+] duplicate-post
 | Location: https://nordvpn.com/wp-content/plugins/duplicate-post/
 | Latest Version: 4.5
 | Last Updated: 2022-10-26T10:09:00.000Z
 |
 | Found By: Known Locations (Aggressive Detection)
 |  - https://nordvpn.com/wp-content/plugins/duplicate-post/, status: 403
 |
 | [!] 3 vulnerabilities identified:
 |
 | [!] Title: Duplicate Post 2.5 - duplicate-post-admin.php User Login Cookie Value SQL Injection
 |     Fixed in: 2.6
 |     References:
 |      - https://wpscan.com/vulnerability/50f04757-f39b-4d34-b945-c5e8fd0c1afb
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-10378
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-10379
 |
 | [!] Title: Duplicate Post 2.5 - options-general.php post Parameter Reflected XSS
 |     Fixed in: 2.6
 |     Reference: https://wpscan.com/vulnerability/4ed4d9c7-d743-4d9d-bc66-fe0122178036
 |
 | [!] Title: Duplicate Post <= 3.2.3 - Authenticated Stored Cross-Site Scripting (XSS)
 |     Fixed in: 3.2.4
 |     References:
 |      - https://wpscan.com/vulnerability/e73b1e92-153a-4f81-ade9-a55ffe603245
 |      - https://packetstormsecurity.com/files/154622/
 |      - https://wordpress.org/support/topic/cross-site-scripting-xss-vulnerability-2/
 |      - https://plugins.trac.wordpress.org/changeset/2208827/duplicate-post
 |
 | The version could not be determined.

[+] ewww-image-optimizer
 | Location: https://nordvpn.com/wp-content/plugins/ewww-image-optimizer/
 | Latest Version: 6.9.3
 | Last Updated: 2022-12-14T18:22:00.000Z
 |
 | Found By: Known Locations (Aggressive Detection)
 |  - https://nordvpn.com/wp-content/plugins/ewww-image-optimizer/, status: 403
 |
 | [!] 2 vulnerabilities identified:
 |
 | [!] Title: EWWW Image Optimizer 2.0.1 - Cross-Site Scripting (XSS)
 |     Fixed in: 2.0.2
 |     References:
 |      - https://wpscan.com/vulnerability/17401705-5f44-47d6-920e-ec058d426114
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-6243
 |      - https://seclists.org/bugtraq/2014/Oct/45
 |      - https://www.immuniweb.com/advisory/HTB23234
 |
 | [!] Title: EWWW Image Optimizer <= 2.8.3 - Remote Code Execution
 |     Fixed in: 2.8.4
 |     References:
 |      - https://wpscan.com/vulnerability/a7a49793-f1ba-483c-9b10-45e0a7ca42e6
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-20010
 |      - https://www.wordfence.com/blog/2016/06/vulnerability-ewww-image-optimizer/
 |      - https://plugins.trac.wordpress.org/changeset/1433803/ewww-image-optimizer
 |
 | The version could not be determined.

[+] one-user-avatar
 | Location: https://nordvpn.com/wp-content/plugins/one-user-avatar/
 | Latest Version: 2.3.9
 | Last Updated: 2022-05-28T20:25:00.000Z
 |
 | Found By: Known Locations (Aggressive Detection)
 |  - https://nordvpn.com/wp-content/plugins/one-user-avatar/, status: 200
 |
 | [!] 2 vulnerabilities identified:
 |
 | [!] Title: One User Avatar < 2.3.7 - Contributor+ Stored Cross-Site Scripting
 |     Fixed in: 2.3.7
 |     References:
 |      - https://wpscan.com/vulnerability/762c506a-f57d-450f-99c0-32d750306ddc
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-24672
 |
 | [!] Title: One User Avatar < 2.3.7 - Avatar Update via CSRF
 |     Fixed in: 2.3.7
 |     References:
 |      - https://wpscan.com/vulnerability/9b9a55d5-c121-4b5b-80df-f9f419c0dc55
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-24675
 |
 | The version could not be determined.

[+] sendgrid-email-delivery-simplified
 | Location: https://nordvpn.com/wp-content/plugins/sendgrid-email-delivery-simplified/
 | Latest Version: 1.11.8
 | Last Updated: 2021-03-04T22:25:00.000Z
 |
 | Found By: Known Locations (Aggressive Detection)
 |  - https://nordvpn.com/wp-content/plugins/sendgrid-email-delivery-simplified/, status: 403
 |
 | [!] 1 vulnerability identified:
 |
 | [!] Title: SendGrid <= 1.11.8 - Authenticated Authorization Bypass
 |     References:
 |      - https://wpscan.com/vulnerability/f3d59d5b-3fc8-47da-95fe-e1d46295e5d2
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-34629
 |      - https://www.wordfence.com/vulnerability-advisories/#CVE-2021-34629
 |
 | The version could not be determined.

[+] stream
 | Location: https://nordvpn.com/wp-content/plugins/stream/
 | Latest Version: 3.9.2
 | Last Updated: 2023-01-10T15:34:00.000Z
 |
 | Found By: Known Locations (Aggressive Detection)
 |  - https://nordvpn.com/wp-content/plugins/stream/, status: 403
 |
 | [!] 3 vulnerabilities identified:
 |
 | [!] Title: Stream <= 3.0.5 - Unauthenticated Events Export
 |     Fixed in: 3.0.6
 |     References:
 |      - https://wpscan.com/vulnerability/8fa150be-75c2-48b3-8d3b-bc623f25debd
 |      - https://wordpress.org/plugins/stream/changelog/
 |
 | [!] Title: Stream < 3.8.2 - Admin+ SQL Injection
 |     Fixed in: 3.8.2
 |     References:
 |      - https://wpscan.com/vulnerability/b9d4f2ad-2f12-4822-817d-982a016af85d
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-24772
 |      - https://plugins.trac.wordpress.org/changeset/2615811/stream
 |
 | [!] Title: Stream < 3.9.2 - Subscriber+ Alert Creation
 |     Fixed in: 3.9.2
 |     References:
 |      - https://wpscan.com/vulnerability/2b506252-6f37-439e-8984-7316d5cca2e5
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-4384
 |
 | The version could not be determined.

[+] WPScan DB API OK
 | Plan: free
 | Requests Done (during the scan): 14
 | Requests Remaining: 47

[+] Finished: Tue Mar 21 14:28:32 2023
[+] Requests Done: 6500
[+] Cached Requests: 27
[+] Data Sent: 3.639 MB
[+] Data Received: 24.703 MB
[+] Memory used: 285.961 MB
[+] Elapsed time: 00:31:52
