import requests
from bs4 import BeautifulSoup

# Define the target URL
url = "http://idmpakistan.pk"

# Make a GET request to the target URL
response = requests.get(url)

# Extract the HTML content from the response
html_content = response.content

# Parse the HTML using BeautifulSoup
soup = BeautifulSoup(html_content, "html.parser")

# Extract the WordPress version from the RSS feed generator
rss_generator = soup.find("generator", text="WordPress")
if rss_generator:
    wp_version = rss_generator.get("version")
    print(f"WordPress version {wp_version} identified")

# Extract the WordPress version from the meta generator tag
meta_generator = soup.find("meta", attrs={"name": "generator"})
if meta_generator:
    wp_version = meta_generator.get("content").split(" ")[0]
    print(f"WordPress version {wp_version} identified")

# Check for known vulnerabilities in the identified WordPress version
if wp_version:
    vulnerabilities_url = f"https://wpscan.com/vulnerabilities/{wp_version}"
    vulnerabilities_response = requests.get(vulnerabilities_url)
    if vulnerabilities_response.status_code == 200:
        print("[+] Known vulnerabilities:")
        soup = BeautifulSoup(vulnerabilities_response.content, "html.parser")
        for vulnerability in soup.select("#vulnerabilities-table tbody tr"):
            title = vulnerability.select_one("td:nth-of-type(1)").text.strip()
            description = vulnerability.select_one("td:nth-of-type(2)").text.strip()
            print(f" - {title}: {description}")
    else:
        print("[-] Unable to check for known vulnerabilities")

# Check for the main theme being used
themes_url = f"{url}/wp-content/themes/"
themes_response = requests.get(themes_url)
if themes_response.status_code == 200:
    soup = BeautifulSoup(themes_response.content, "html.parser")
    main_theme = soup.select_one("a[href*='/wp-content/themes/']:not([href*='/wp-content/themes/..'])")
    if main_theme:
        print(f"[+] Main theme: {main_theme.text.strip()}")
    else:
        print("[-] The main theme could not be detected")
else:
    print("[-] Unable to check for the main theme")

# Make recommendations based on the findings
if wp_version:
    if "Outdated" in wp_version:
        print("[!] The WordPress version is outdated")
        print("    - Upgrade to the latest version")
    else:
        print("[+] The WordPress version is up to date")
else:
    print("[-] Unable to determine the WordPress version")

if main_theme:
    print("[!] The main theme is being used")
    print("    - Make sure it is up to date")
else:
    print("[+] No main theme detected")

print("[+] Done")
